package com.adragovcic.arsfuturazadatak.utils

import java.lang.Math.abs
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


fun getElapsedArticleTime(time: String): String {
    val parsedTimeString = time.replace("T", " ").replace("Z", "")
    val formatter = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH)
    val articleDate = formatter.parse(parsedTimeString)
    val currentDate = Date(System.currentTimeMillis())
    val diffInMillies = abs(currentDate.time - articleDate.time)
    val diffDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS)
    val diffHours = TimeUnit.HOURS.convert(diffInMillies, TimeUnit.MILLISECONDS)
    val diffMins = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS)

    if (diffDays != 0L) return "$diffDays days"
    if (diffHours != 0L) return "$diffHours hours"
    if (diffMins != 0L) return "$diffMins minutes"

    return ""
}