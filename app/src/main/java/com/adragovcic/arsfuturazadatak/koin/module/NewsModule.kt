package com.adragovcic.arsfuturazadatak.koin.module

import com.adragovcic.arsfuturazadatak.network.NewsAPI
import com.adragovcic.arsfuturazadatak.repository.NewsRepository
import com.adragovcic.arsfuturazadatak.viewModel.NewsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val newsModule = module {
    single { provideNewsRepository(get()) }
    viewModel { NewsViewModel(get()) }
}

fun provideNewsRepository(newsAPI: NewsAPI) = NewsRepository(newsAPI)
