package com.adragovcic.arsfuturazadatak.koin.module

import com.adragovcic.arsfuturazadatak.network.NewsAPI
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { provideRetrofit() }
    single { provideNewsAPI(get()) }
}

fun provideNewsAPI(retrofit: Retrofit): NewsAPI {
    return retrofit.create(NewsAPI::class.java)
}

fun provideRetrofit(): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://newsapi.org/v2/")
        .build()
}
