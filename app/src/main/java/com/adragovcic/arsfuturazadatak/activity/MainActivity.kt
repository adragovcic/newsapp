package com.adragovcic.arsfuturazadatak.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adragovcic.arsfuturazadatak.R
import com.adragovcic.arsfuturazadatak.view.recyclerView.RecycleViewItem
import com.adragovcic.arsfuturazadatak.view.recyclerView.RecycleViewViewType
import com.adragovcic.arsfuturazadatak.view.recyclerView.adapter.NewsAdapter
import com.adragovcic.arsfuturazadatak.viewModel.NewsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    val newsViewModel: NewsViewModel by viewModel()
    private var newsAdapter: NewsAdapter = NewsAdapter(ArrayList(), this)
    private val debouncer: PublishSubject<String> = PublishSubject.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolBar)

        configureSearchBar()
        showTopHeadlines()
        showLatestNews()

        with(recyclerView) {
            adapter = newsAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            //pagination
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                var loading = false
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (dy > 0 && !loading && !canScrollVertically(RecyclerView.FOCUS_DOWN) &&
                        editText_search_bar.text.toString().isNotEmpty()
                    ) {
                        loading = true
                        newsAdapter.showProgressBar()
                        recyclerView.scrollToPosition(newsAdapter.itemCount - 1)
                        Handler().postDelayed({
                            newsViewModel.getSearchNewsPageByQuery(
                                editText_search_bar.text.toString(),
                                newsViewModel.nextPage
                            )
                                .observe(this@MainActivity, Observer {
                                    if (it.errorMessage.isNotEmpty()) {
                                        Toast.makeText(this@MainActivity, it.errorMessage, Toast.LENGTH_LONG).show()
                                    } else {
                                        it.data?.let { data ->
                                            if (data.articles.isNotEmpty()) {
                                                newsAdapter.loadNextVerticalPage(data.articles)
                                                newsViewModel.nextPage++
                                            }
                                        }
                                    }
                                    newsAdapter.hideProgressBar()
                                    loading = false
                                })
                        }, 300)
                    }
                }
            })
        }

        editText_search_bar.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isEmpty()) {
                    newsViewModel.isShowSearchResults = false
                    newsAdapter.list.clear()
                    newsAdapter.notifyDataSetChanged()
                    showTopHeadlines()
                    showLatestNews()
                } else {
                    newsViewModel.isShowSearchResults = true
                    newsViewModel.nextPage = 2
                    debouncer.onNext(s.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
    }

    private fun showTopHeadlines() {
        newsAdapter.showProgressBar()
        newsViewModel.getTopHeadlines().observe(this, Observer {
            if (it != null) {
                if (it.errorMessage.isNotEmpty()) {
                    Toast.makeText(this, it.errorMessage, Toast.LENGTH_LONG).show()
                } else {
                    it.data?.let { data ->
                        newsAdapter.list.add(
                            0,
                            RecycleViewItem(resources.getString(R.string.top_headlines), RecycleViewViewType.TITLE)
                        )
                        newsAdapter.list.add(1, RecycleViewItem(data.articles, RecycleViewViewType.HORIZONTAL))
                        newsAdapter.notifyDataSetChanged()
                    }
                }
            }
            newsAdapter.hideProgressBar()
        })
    }

    private fun showLatestNews() {
        newsAdapter.showProgressBar()
        newsViewModel.getLatestNews().observe(this, Observer {
            if (it != null) {
                if (it.errorMessage.isNotEmpty()) {
                    Toast.makeText(this, it.errorMessage, Toast.LENGTH_LONG).show()
                } else {
                    it.data?.let { data ->
                        newsAdapter.list.add(
                            RecycleViewItem(
                                resources.getString(R.string.latest_news),
                                RecycleViewViewType.TITLE
                            )
                        )
                        newsAdapter.list.add(RecycleViewItem(data.articles, RecycleViewViewType.VERTICAL))
                        newsAdapter.notifyDataSetChanged()
                    }
                }
            }
            newsAdapter.hideProgressBar()
        })
    }

    @SuppressLint("CheckResult")
    private fun configureSearchBar() {
        debouncer
            .debounce(300, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { query ->
                newsViewModel.getSearchNewsPageByQuery(query, 1).observe(this@MainActivity, Observer {
                    if (it != null && newsViewModel.isShowSearchResults) {
                        if (it.errorMessage.isNotEmpty()) {
                            Toast.makeText(this@MainActivity, it.errorMessage, Toast.LENGTH_LONG).show()
                        } else {
                            it.data?.let { data ->
                                newsAdapter.list.clear()
                                newsAdapter.list.add(
                                    RecycleViewItem(
                                        resources.getString(R.string.search_results),
                                        RecycleViewViewType.TITLE
                                    )
                                )
                                newsAdapter.list.add(RecycleViewItem(data.articles, RecycleViewViewType.VERTICAL))
                                newsAdapter.notifyDataSetChanged()
                                recyclerView.scrollToPosition(0)
                            }
                        }
                    }
                })
            }

    }
}
