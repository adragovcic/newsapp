package com.adragovcic.arsfuturazadatak

import android.app.Application
import com.adragovcic.arsfuturazadatak.koin.module.networkModule
import com.adragovcic.arsfuturazadatak.koin.module.newsModule
import org.koin.core.context.startKoin

class NewsApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(networkModule)
            modules(newsModule)
        }
    }
}