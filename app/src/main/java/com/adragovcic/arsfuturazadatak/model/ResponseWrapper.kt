package com.adragovcic.arsfuturazadatak.model

class ResponseWrapper<out T>(
    val data: T?,
    val errorMessage: String
) {
     data class Builder<T>(
        private var data: T? = null,
        private var errorMessage: String = ""
    ) {
        fun data(data: T?) = apply { this.data = data }
        fun errorMessage(errorMessage: String) = apply { this.errorMessage = errorMessage }
        fun build() = ResponseWrapper(data, errorMessage)
    }
}