package com.adragovcic.arsfuturazadatak.model

data class Source(
    val id: String,
    val name: String
)
