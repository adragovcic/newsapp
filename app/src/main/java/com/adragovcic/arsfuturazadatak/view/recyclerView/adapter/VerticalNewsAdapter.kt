package com.adragovcic.arsfuturazadatak.view.recyclerView.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.adragovcic.arsfuturazadatak.R
import com.adragovcic.arsfuturazadatak.model.Article
import com.adragovcic.arsfuturazadatak.utils.getElapsedArticleTime
import kotlinx.android.synthetic.main.vertical_list_item.view.*

class VerticalNewsAdapter(private val items: ArrayList<Article>, private val context: Context) :
    RecyclerView.Adapter<VerticalNewsAdapter.VerticalListViewHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VerticalListViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.vertical_list_item, parent, false)
        return VerticalListViewHolder(v)
    }

    override fun onBindViewHolder(holder: VerticalListViewHolder, position: Int) {
        holder.titleTextView.text = items[holder.adapterPosition].title
        holder.descriptionTextView.text = items[holder.adapterPosition].description
        holder.timeTextView.text = context.getString(
            R.string.vertical_article_time_placeholder,
            getElapsedArticleTime(items[holder.adapterPosition].publishedAt)
        )
        holder.sourceTextView.text = items[holder.adapterPosition].source.name
    }

    inner class VerticalListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleTextView: TextView = view.textView_vertical_list_item_title
        val descriptionTextView: TextView = view.textView_vertical_list_item_description
        val timeTextView: TextView = view.textView_vertical_list_item_time
        val sourceTextView: TextView = view.textView_vertical_list_item_source_name
    }
}
