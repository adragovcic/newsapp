package com.adragovcic.arsfuturazadatak.view.recyclerView.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.adragovcic.arsfuturazadatak.R
import com.adragovcic.arsfuturazadatak.model.Article
import com.adragovcic.arsfuturazadatak.utils.getElapsedArticleTime
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.horizontal_list_item.view.*

class HorizontalNewsAdapter(private val items: ArrayList<Article>, private val context: Context) :
    RecyclerView.Adapter<HorizontalNewsAdapter.HorizontalListViewHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HorizontalListViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.horizontal_list_item, parent, false)
        return HorizontalListViewHolder(v)
    }

    override fun onBindViewHolder(holder: HorizontalListViewHolder, position: Int) {
        Picasso.get().load(items[holder.adapterPosition].urlToImage).fit().into(holder.image)
        holder.titleTextView.text = items[holder.adapterPosition].title
        holder.timeTextView.text = context.getString(
            R.string.horizontal_article_time_placeholder,
            getElapsedArticleTime(items[holder.adapterPosition].publishedAt)
        )
        holder.sourceTextView.text = items[holder.adapterPosition].source.name
    }

    inner class HorizontalListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.imageView_horizontal_list_item_image
        val titleTextView: TextView = view.textView_horizontal_list_item_title
        val timeTextView: TextView = view.textView_horizontal_list_item_time
        val sourceTextView: TextView = view.textView_horizontal_list_item_source_name
    }
}