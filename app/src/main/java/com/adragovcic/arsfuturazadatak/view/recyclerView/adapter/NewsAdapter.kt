package com.adragovcic.arsfuturazadatak.view.recyclerView.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adragovcic.arsfuturazadatak.R
import com.adragovcic.arsfuturazadatak.model.Article
import com.adragovcic.arsfuturazadatak.view.recyclerView.RecycleViewItem
import com.adragovcic.arsfuturazadatak.view.recyclerView.RecycleViewViewType


class NewsAdapter(val list: ArrayList<RecycleViewItem>, private val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val HEADER_TITLE = 0
        const val HORIZONTAL = 1
        const val VERTICAL = 2
        const val PROGRESS_BAR = 3
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v: View
        return when (viewType) {
            HORIZONTAL -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.horizontal_recycleview, parent, false)
                HorizontalViewHolder(v)
            }
            VERTICAL -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.vertical_recycleview, parent, false)
                VerticalViewHolder(v)
            }
            HEADER_TITLE -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.header_title, parent, false)
                HeaderTitleViewHolder(v)
            }
            PROGRESS_BAR -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.progress_bar, parent, false)
                ProgressBarViewHolder(v)
            }
            else -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.header_title, parent, false)
                HeaderTitleViewHolder(v)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is VerticalViewHolder -> {
                val verticalNewsAdapter = VerticalNewsAdapter(list[position].content as ArrayList<Article>, context)
                holder.recyclerView.adapter = verticalNewsAdapter
                holder.recyclerView.layoutManager = LinearLayoutManager(context)
            }
            is HorizontalViewHolder -> {
                val horizontalViewAdapter = HorizontalNewsAdapter(list[position].content as ArrayList<Article>, context)
                holder.recyclerView.adapter = horizontalViewAdapter
                holder.recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }

            is HeaderTitleViewHolder -> {
                holder.title.text = list[position].content as String
            }

        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = list[position]
        return when (item.recycleViewViewType) {
            RecycleViewViewType.HORIZONTAL -> HORIZONTAL
            RecycleViewViewType.VERTICAL -> VERTICAL
            RecycleViewViewType.TITLE -> HEADER_TITLE
            RecycleViewViewType.PROGRESS_BAR -> PROGRESS_BAR
            else -> -1
        }
    }

    fun loadNextVerticalPage(data: List<Article>) {
        val verticalItem = list.find { it.recycleViewViewType == RecycleViewViewType.VERTICAL }
        (verticalItem?.content as ArrayList<Article>).addAll(data)
        notifyDataSetChanged()
    }

    fun showProgressBar() {
        if(list.find { it.recycleViewViewType == RecycleViewViewType.PROGRESS_BAR } == null){
            list.add(RecycleViewItem("", RecycleViewViewType.PROGRESS_BAR))
            notifyItemInserted(list.size - 1)
        }
    }

    fun hideProgressBar() {
        list.remove(list.find { it.recycleViewViewType == RecycleViewViewType.PROGRESS_BAR })
        notifyItemRemoved(list.size)
    }

    inner class HorizontalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val recyclerView: RecyclerView = itemView.findViewById(R.id.recycleView_horizontal)
    }

    inner class VerticalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val recyclerView: RecyclerView = itemView.findViewById(R.id.recycleView_vertical)
    }

    inner class HeaderTitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.textView_articles_title)
    }

    inner class ProgressBarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val progressBar: ProgressBar = itemView.findViewById(R.id.progress_bar)
    }

}


