package com.adragovcic.arsfuturazadatak.view.recyclerView

data class RecycleViewItem(
    val content: Any,
    val recycleViewViewType: RecycleViewViewType?
)