package com.adragovcic.arsfuturazadatak.view.recyclerView

enum class RecycleViewViewType {
    VERTICAL, HORIZONTAL, TITLE, PROGRESS_BAR
}