package com.adragovcic.arsfuturazadatak.network

import com.adragovcic.arsfuturazadatak.model.Article

data class APIResponse(
    val status: String,
    val totalResults: Int,
    val articles: List<Article>,
    val message: String
)

