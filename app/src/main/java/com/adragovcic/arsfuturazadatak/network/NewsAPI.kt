package com.adragovcic.arsfuturazadatak.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsAPI {

    companion object {
        const val API_KEY = "e2324bb456f7481f9f6ff60547617d64"
        const val API_KEY_2 = "8ef63b5ad8084fcc9f7aa6429de29141"
        const val API_KEY_3 = "55ff4382dc904c2cbbc6c87a2e3cd345"
    }

    @GET("top-headlines?sources=bbc-news&pageSize=10&page=1&apiKey=$API_KEY_3")
    fun getTopHeadlines(): Call<APIResponse>

    @GET("top-headlines?country=us&apiKey=$API_KEY_3")
    fun getLatestNews(): Call<APIResponse>

    @GET("everything?pageSize=10&apiKey=$API_KEY_3")
    fun getSearchNewsByQuery(@Query("q") searchQuery: String, @Query("page") page: Int): Call<APIResponse>
}