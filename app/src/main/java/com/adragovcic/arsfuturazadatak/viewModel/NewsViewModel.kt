package com.adragovcic.arsfuturazadatak.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adragovcic.arsfuturazadatak.model.ResponseWrapper
import com.adragovcic.arsfuturazadatak.network.APIResponse
import com.adragovcic.arsfuturazadatak.repository.NewsRepository

class NewsViewModel(private val repository: NewsRepository) : ViewModel() {

    private lateinit var topHeadlines: MutableLiveData<ResponseWrapper<APIResponse>>
    private lateinit var latestNews: MutableLiveData<ResponseWrapper<APIResponse>>
    var nextPage = 2
    var isShowSearchResults = true

    init {
        reloadNews()
    }

    fun getSearchNewsPageByQuery(searchQuery: String, page: Int): MutableLiveData<ResponseWrapper<APIResponse>> {
        return repository.getSearchNewsPageByQuery(searchQuery, page)
    }

    fun getTopHeadlines(): MutableLiveData<ResponseWrapper<APIResponse>> {
        return topHeadlines
    }

    fun getLatestNews(): MutableLiveData<ResponseWrapper<APIResponse>> {
        return latestNews
    }

    private fun loadTopHeadlines() {
        topHeadlines = repository.getTopHeadlines()
    }

    private fun loadLatestNews() {
        latestNews = repository.getLatestNews()
    }

    private fun reloadNews() {
        loadTopHeadlines()
        loadLatestNews()
    }
}