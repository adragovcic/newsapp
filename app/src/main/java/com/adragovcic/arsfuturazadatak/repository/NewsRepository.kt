package com.adragovcic.arsfuturazadatak.repository

import androidx.lifecycle.MutableLiveData
import com.adragovcic.arsfuturazadatak.model.ResponseWrapper
import com.adragovcic.arsfuturazadatak.network.APIResponse
import com.adragovcic.arsfuturazadatak.network.NewsAPI
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NewsRepository(private val newsAPI: NewsAPI) {

    fun getTopHeadlines(): MutableLiveData<ResponseWrapper<APIResponse>> {
        val liveData = MutableLiveData<ResponseWrapper<APIResponse>>()
        val retrofitCall = newsAPI.getTopHeadlines()
        retrofitCall.enqueue(object : Callback<APIResponse> {
            override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                liveData.value = consumeAPIResponse(response)
            }

            override fun onFailure(call: Call<APIResponse>, t: Throwable?) {
                liveData.value = consumeAPIResponse(t)
            }
        })

        return liveData
    }

    fun getLatestNews(): MutableLiveData<ResponseWrapper<APIResponse>> {
        val liveData: MutableLiveData<ResponseWrapper<APIResponse>> = MutableLiveData()
        val retrofitCall = newsAPI.getLatestNews()
        retrofitCall.enqueue(object : Callback<APIResponse> {
            override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                liveData.value = consumeAPIResponse(response)
            }

            override fun onFailure(call: Call<APIResponse>, t: Throwable?) {
                liveData.value = consumeAPIResponse(t)
            }
        })

        return liveData
    }

    fun getSearchNewsPageByQuery(searchQuery: String, page: Int): MutableLiveData<ResponseWrapper<APIResponse>> {
        val liveData: MutableLiveData<ResponseWrapper<APIResponse>> = MutableLiveData()
        val retrofitCall = newsAPI.getSearchNewsByQuery(searchQuery, page)
        retrofitCall.enqueue(object : Callback<APIResponse> {
            override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                liveData.value = consumeAPIResponse(response)
            }

            override fun onFailure(call: Call<APIResponse>, t: Throwable?) {
                liveData.value = consumeAPIResponse(t)
            }
        })
        return liveData
    }


    private fun consumeAPIResponse(response: Response<APIResponse>): ResponseWrapper<APIResponse> {
        val responseBuilder = ResponseWrapper.Builder<APIResponse>()
        if (response.isSuccessful) {
            if (response.body() != null) {
                responseBuilder.data(response.body())
            } else if (response.errorBody() != null) {
                val jObjError = JSONObject(response.errorBody()!!.string())
                responseBuilder.errorMessage(jObjError.getString("message"))
            }
        } else {
            responseBuilder.errorMessage("Response code: ${response.code()} ${response.message()}")
        }

        return responseBuilder.build()
    }

    private fun consumeAPIResponse(t: Throwable?): ResponseWrapper<APIResponse> {
        //handle exception
        return ResponseWrapper.Builder<APIResponse>().errorMessage("Network error").build()
    }


}
